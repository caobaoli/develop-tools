import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xhrd.generator.GeneratorFacade;
import com.xhrd.generator.GeneratorProperties;

public class GeneratorMain {
    /**
     * 日志对象
     */
    private static Log logger = LogFactory.getLog(GeneratorMain.class);

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            GeneratorFacade generatorFacade = new GeneratorFacade();
            // g.printAllTableNames(); //打印数据库中的表名称

            generatorFacade.deleteOutRootDir(); // 删除生成器的输出目录
            generatorFacade.generateByTable("channel_info", "template");
            // 通过数据库表生成文件,template为模板的根目录
            // g.generateByAllTable("template");
            // 自动搜索数据库中的所有表并生成文件,template为模板的根目录
            // g.generateByClass(Blog.class,"template_clazz");
            // 删除生成的文件
            // g.deleteByTable("table_name", "template");
            // 打开文件夹
            Runtime.getRuntime().exec("cmd.exe /c start " + GeneratorProperties.getRequiredProperty("outRoot"));

        } catch (Exception e) {
            logger.error("generate", e);
        }
    }
}

<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basepackage}.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${basepackage}.service.EntityServiceImpl;
import ${basepackage}.dao.${className}DAO;
import ${basepackage}.entities.${className};
import ${basepackage}.service.${className}Service;
import ${basepackage}.vo.${className}SearchCondition;
import ${basepackage}.framework.dao.BaseDAO;
/**
 * ${className} service实现类
 *
 * @author bobby
 * @version 1.0
 *
 */
@Service(value = "${classNameLower}Service")
public class ${className}ServiceImpl extends EntityServiceImpl<${className}, ${className}SearchCondition> implements ${className}Service {

    @Autowired
    public void set${className}Dao(BaseDAO<${className}, Serializable> ${classNameLower}Dao) {
        super.setDao(${classNameLower}DAO);
    }
}

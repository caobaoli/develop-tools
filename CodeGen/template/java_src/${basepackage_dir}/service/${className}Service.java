<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basepackage}.service;

import org.springframework.stereotype.Service;

import ${basepackage}.service.EntityService;
import ${basepackage}.entities.${className};
import ${basepackage}.vo.${className}SearchCondition;

/**
 * ${className} service接口
 *
 * @author bobby
 * @version 1.0
 */
public interface ${className}Service extends EntityService<${className}, ${className}SearchCondition>{

}


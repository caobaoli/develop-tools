<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basepackage}.dao;

import org.springframework.stereotype.Repository;

import ${basepackage}.dao.${className}DAO;
import ${basepackage}.entities.${className};
import ${basepackage}.framework.dao.mybatis.BaseMyBatisDAOImpl;

/**
 * ${className}DAO
 *
 * @author bobby
 * @version 1.0
 */
@Repository(value = "${classNameLower}Dao")
public class ${className}DAOImpl extends BaseMyBatisDAOImpl<${className}, String> implements ${className}DAO {

}
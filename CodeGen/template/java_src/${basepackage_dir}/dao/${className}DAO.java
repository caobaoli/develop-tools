<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
package ${basepackage}.dao;

import java.io.Serializable;
import org.springframework.stereotype.Repository;
import ${basepackage}.entities.${className};
import ${basepackage}.framework.dao.BaseDAO;

/**
 * ${className}DAO
 *
 * @author bobby
 * @version 1.0
 */

public interface ${className}DAO extends BaseDAO<${className}, Serializable> {

}